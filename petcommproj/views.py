import os, sys
import json, http.client

from django.http import HttpResponse
from django.shortcuts import render
from parse_rest.datatypes import Function, GeoPoint
from parse_rest.datatypes import Object as ParseObject
from parse_rest.connection import register, SessionToken
from parse_rest.query import QueryResourceDoesNotExist
from parse_rest.connection import ParseBatcher
from parse_rest.core import ResourceRequestBadRequest, ParseError
from django.views.decorators.csrf import csrf_exempt
from parse_rest.user import User
from django.template import loader
from django.shortcuts import redirect
from django.conf import settings
	
class pets(ParseObject):
    pass
class profile(ParseObject):
    pass
	

	
# Login page
def userlogin(request):
    if request.method == 'GET':
        context = ""
        return render(request, 'Login_Register.html', context)

    elif request.method == 'POST':
        email = request.POST.get('email', '')
        password = request.POST.get('password', '')
        register(settings.PARSE_APPID, settings.PARSE_APIKEY)
        try:
            u = User.login(email, password)
        except:
            u = None
        if (u is None):
            return render(request, 'Login_Register.html', {'flashmessage': "Wrong email or password"})
        else:     
            token = u.sessionToken
            request.session['mysession_token'] = token
            return redirect('../createprofile')

# Viewing pets of a user			
def viewpets(request):
    token = request.session.get('mysession_token')
    if (token is None):
        return HttpResponse("token is none.")
	
    register(settings.PARSE_APPID, settings.PARSE_APIKEY)    
    me=None
    with SessionToken(token):
        user_url = '/'.join([PARSE_API_ROOT, 'users/me'])
        me = User.GET(user_url)
    if (me is None):
        return render(request, 'Login_Register.html', {'flashmessage': "Please login to view your pets"})
    else:
        username = me['username']
        all_pets = pets.Query.all()
        user_pets = all_pets.filter(userid="_User$%s" % me['objectId'])
        return render(request, 'pets.html', {'username': username, 'pets': user_pets})		

# Creating and editing user profiles		
def createprofile(request):
	me = getUserObject(request)
	if (me is None):
		return render(request, 'Login_Register.html', {'flashmessage': "Please login first to access"})
	else:
		try:
			#determine if the user already has a profile
			p = profile.Query.get(userid="_User$%s" % me['objectId'])
		except QueryResourceDoesNotExist:
			p = None		
		if request.method == 'GET':
			if (p is not None):
				return render(request, 'CreateProfile.html', {'userfields': me, 'profilefields':p})		
			else:
				return render(request, 'CreateProfile.html', {'userfields': me})		
		elif request.method == 'POST':
			if (p is None):
				#if profile does not exist than create a profile for this user
				p = profile()			
			#if profile exists for the user then update
			p.businessname = request.POST.get('businessname', '')
			p.homelocation = request.POST.get('homelocation', '')
			p.city = request.POST.get('city', '')
			p.country = request.POST.get('country', '')
			p.province = request.POST.get('province', '')
			p.postalcode = request.POST.get('postalcode', '')
			p.shortdescription = request.POST.get('shortdescription', '')
			p.userid = {"__type": "Pointer", "className": "_User", "objectId": me['objectId']}
			p.save()	
			return render(request, 'CreateProfile.html', {'flashmessage': "Profile saved", 'userfields': me, 'profilefields': p})		

# Gets the User Object from session		
def getUserObject(request):
    token = request.session.get('mysession_token')
    if (token is None):
        return None
    register(settings.PARSE_APPID, settings.PARSE_APIKEY)    
    me=None
    with SessionToken(token):
        user_url = '/'.join([settings.PARSE_API_ROOT, 'users/me'])
        me = User.GET(user_url)
    return me

# Main landing page	
def index(request):	
	return render(request, 'LandingPage.html')
