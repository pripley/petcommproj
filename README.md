### What is this repository for? ###

A proof of concept django app that uses a parse python module call parse_rest (
https://github.com/milesrichardson/ParsePy) to connect with back4apps.com database

### How do I get set up? ###
Developed and tested with Python3.4
Django==1.10.3
parse-rest==0.2.20141004