Django==1.10.3
django-mongodb-engine==0.6.0
djangotoolbox==1.8.0
gunicorn==19.6.0
parse-rest==0.2.20141004
pycurl==7.43.0
pymongo==3.3.1
six==1.10.0
